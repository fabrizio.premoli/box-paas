# cd-in-a-box

A sample dev environment to experiment DevOps tasks.

## Installation

Required:
- git: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
- VirtualBox platform: https://www.virtualbox.org/wiki/Downloads (> 6.0)
- HashiCorp Vagrant: https://www.vagrantup.com/docs/installation/ (> 2.2)

Suggested:
- vagrant-hostmanager: https://github.com/devopsgroup-io/vagrant-hostmanager (> 1.8)
- Oracle VM VirtualBox Extension Pack: https://www.virtualbox.org/wiki/Downloads (> 6.0)

## Provision

**Clone the repository**

- Choose a directory on your computer, e.g. `~/dev/`
- Open a terminal there (a Bash shell is fine)
- Get the repo, `git clone https://gitlab.com/cd-in-a-box/box-paas.git box-paas/`

**Create the Virtual Machine**

The default configuration is using 4 CPUs and 6GB of RAM. You can tune it from `Vagrantfile`.

In the terminal, run:
- `cd box-paas/`
- `vagrant up`

In order to resolve the custom `ws.so` URLs, add the contents of `provision/hosts` to your `/etc/hosts`.

## Usage

Once provision is finished, you can login login:
- `vagrant ssh` for a regular SSH connection
- `ssh -D 8081 -l vagrant 192.168.56.10` if you need a SOCKS5 tunnel

The user is `vagrant` with password `vagrant`. To became root run `sudo -s`.

### Services

To check running services from the terminal use `nomad status`.

Exposed services:
```sh
** Nomad        > http://nomad.ws.so/
** Consul       > http://consul.ws.so/
** Fabio        > http://fabio.ws.so/
** Registry     > http://registry.ws.so/v2/
** Prometheus   > http://prometheus.ws.so/
** Grafana      > http://grafana.ws.so/ [user: admin, pass: admin]
** Gogs         > http://git.ws.so/ [user: gogs, pass: gogs]
** Jenkins      > http://jenkins.ws.so/ [user: jenkins, pass: jenkins]
```

### Jenkins executors

You can re-build Jenkins' executors running `provision/jenkins/build-executors.sh`.


