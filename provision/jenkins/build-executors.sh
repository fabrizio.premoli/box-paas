#!/bin/bash -e

echo "Building base Jenkins executor images..."
# Build generic docker-in-docker slave image
(
    cd /vagrant/provision/jenkins/executors/jenkins-slave-dind
    docker build --rm -t registry.ws.so/jenkins-slave-dind .
    docker push registry.ws.so/jenkins-slave-dind
)

# Build docker-in-docker slave image with PHP
(
    cd /vagrant/provision/jenkins/executors/jenkins-slave-dind-php
    docker build --rm -t registry.ws.so/jenkins-slave-dind-php .
    docker push registry.ws.so/jenkins-slave-dind-php
)

# Build docker-in-docker slave image with nomad
(
    cd /vagrant/provision/jenkins/executors/docker-nomad
    docker build --rm -t registry.ws.so/docker-nomad .
    docker push registry.ws.so/docker-nomad
)