#!/bin/bash -e

# nomad job descriptions
mkdir -p /etc/nomad.jobs
chmod 755 /etc/nomad.jobs

# enable and start jobs-as-services via systemd
for svc in fabio haproxy registry prometheus grafana gogs
do
    echo "Enable and restart ${svc}"
    cp -va /vagrant/provision/nomad/jobs/${svc}.nomad /etc/nomad.jobs/
    cp -va /vagrant/provision/nomad/systemd/nomad-job.service /etc/systemd/system/${svc}.service
    systemctl enable ${svc}.service
    systemctl restart ${svc}.service
    sleep 1
done